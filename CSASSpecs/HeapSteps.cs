﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Classes;
using Csas.Algorithms;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class HeapSteps
    {
        int[] data;
        int[] sortedData;
        BinaryHeap<int> heap;

        [Given(@"I have entered data into array:")]
        public void GivenIHaveEnteredDataIntoArray(Table table)
        {
            data = new int[table.Rows.Count];
            int currentIndex = 0;

            foreach (TableRow value in table.Rows)
            {
                data[currentIndex++] = value.GetInt32("Value");
            }
        }

        [When(@"I add array into heap")]
        public void WhenIAddArrayIntoHeap()
        {
            heap = new BinaryHeap<int>(data, HeapOrder.Ascending, null);
        }

        [Then(@"heap should be like:")]
        public void ThenHeapShouldBeLike(Table table)
        {
            int currentIndex = 0;
            foreach (TableRow value in table.Rows)
            {
                Assert.AreEqual(value.GetInt32("Value"), heap.Vertices[currentIndex]);
                currentIndex++;
            }
        }

        [Then(@"root chilren should be (.*) and (.*)")]
        public void ThenRootChilrenShouldBeAnd(int p0, int p1)
        {
            Assert.AreEqual(p0, heap.Vertices[1]);
            Assert.AreEqual(p1, heap.Vertices[2]);
        }

        [When(@"I sort array with heap")]
        public void WhenISortArrayWithHeap()
        {
            HeapSort<int> heapSort = new HeapSort<int>(data);
            sortedData = heapSort.Sort();
        }

        [Then(@"array should be sorted")]
        public void ThenArrayShouldBeSorted()
        {
            Assert.NotNull(sortedData);

            for (int i = 0; i < sortedData.Length; i++) 
            {
                Console.WriteLine("{0}", sortedData[i]);
            }

            for (int i = 0; i < sortedData.Length - 1; i++)
            {
                Assert.True(sortedData[i].CompareTo(sortedData[i + 1]) <= 0);
            }
        }

        [When(@"I sort array with heap descending")]
        public void WhenISortArrayWithHeapDescending()
        {
            HeapSort<int> heapSort = new HeapSort<int>(data);
            sortedData = heapSort.Sort(HeapOrder.Descending);
        }

        [Then(@"array should be sorted descending")]
        public void ThenArrayShouldBeSortedDescending()
        {
            Assert.NotNull(sortedData);

            for (int i = 0; i < sortedData.Length - 1; i++)
            {
                Assert.True(sortedData[i].CompareTo(sortedData[i + 1]) >= 0);
            }
        }


    }
}
