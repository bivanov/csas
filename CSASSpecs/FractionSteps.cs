﻿using System;
using System.Reflection;
using TechTalk.SpecFlow;

using Csas.Classes;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class FractionSteps
    {
        private Fraction fraction;
        private Fraction operationFraction1;
        private Fraction operationFraction2;
        private int comparisonResult;

        [Given(@"I have created fraction with values (.*) and (.*)")]
        public void GivenIHaveCreatedFractionWithValuesAnd(int p0, int p1)
        {
            fraction = new Fraction(p0, p1);
        }

        [Then(@"float representation should be near (.*)")]
        public void ThenFloatRepresentationShouldBeNear(decimal p0)
        {
            Assert.Less(Math.Abs((float)p0 - (float)fraction), 0.001f);
        }

        [Then(@"int representation should be (.*)")]
        public void ThenIntRepresentationShouldBe(int p0)
        {
            Assert.AreEqual(p0, (int)fraction);
        }

        [Given(@"I have created fraction with float (.*)")]
        public void GivenIHaveCreatedFractionWithFloat(Decimal p0)
        {
            fraction = new Fraction((float)p0);
        }

        [Given(@"I have created fraction with string '(.*)'")]
        public void GivenIHaveCreatedFractionWithString(string p0)
        {
            try
            {
                fraction = new Fraction(p0);
            }
            catch (Exception e)
            {
                ScenarioContext.Current.Add("Exception_InvalidArgument", e);
            }
            
        }

        [Then(@"string representation should be '(.*)'")]
        public void ThenStringRepresentationShouldBe(string p0)
        {
            Assert.AreEqual(p0, fraction.ToString());
        }

        [Given(@"I have created one fraction with values (.*) and (.*)")]
        public void GivenIHaveCreatedOneFractionWithValuesAnd(int p0, int p1)
        {
            operationFraction1 = new Fraction(p0, p1);
        }

        [Given(@"I have created second fraction with values (.*) and (.*)")]
        public void GivenIHaveCreatedSecondFractionWithValuesAnd(int p0, int p1)
        {
            operationFraction2 = new Fraction(p0, p1);
        }

        [When(@"I calculate sum of these fractions")]
        public void WhenICalculateSumOfTheseFractions()
        {
            fraction = operationFraction1 + operationFraction2;
        }

        [When(@"I calculate difference of these fractions")]
        public void WhenICalculateDifferenceOfTheseFractions()
        {
            fraction = operationFraction1 - operationFraction2;
        }

        [When(@"I calculate product of these fractions")]
        public void WhenICalculateProductOfTheseFractions()
        {
            fraction = operationFraction1 * operationFraction2;
        }

        [When(@"I calculate quotient of these fractions")]
        public void WhenICalculateQuotientOfTheseFractions()
        {
            fraction = operationFraction1 / operationFraction2;
        }

        [Then(@"exception ""(.*)"" should occur")]
        public void ThenExceptionShouldOccur(string p0)
        {
            var exception = ScenarioContext.Current[p0];
            Assert.That(exception, Is.Not.Null);
        }

        [When(@"I compare these fractions")]
        public void WhenICompareTheseFractions()
        {
            comparisonResult = operationFraction1.CompareTo(operationFraction2);
        }

        [Then(@"comparison result should be positive")]
        public void ThenComparisonResultShouldBePositive()
        {
            Assert.That(comparisonResult > 0);
        }

        [Then(@"comparison result should be negative")]
        public void ThenComparisonResultShouldBeNegative()
        {
            Assert.That(comparisonResult < 0);
        }

        [Given(@"I have created second fraction as copy of first")]
        public void GivenIHaveCreatedSecondFractionAsCopyOfFirst()
        {
            operationFraction2 = new Fraction(operationFraction1);
        }

        [Then(@"comparison result should be (.*)")]
        public void ThenComparisonResultShouldBe(int p0)
        {
            Assert.That(comparisonResult == p0);
        }
    }
}