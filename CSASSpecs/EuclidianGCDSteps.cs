﻿using System;
using TechTalk.SpecFlow;

using Csas.Algorithms;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class EuclidianGCDSteps
    {
        int result;
        int value1;
        int value2;

        [Given(@"I have entered (.*) and (.*)")]
        public void GivenIHaveEnteredAnd(int p0, int p1)
        {
            value1 = p0;
            value2 = p1;
        }

        [When(@"I calculate GCD")]
        public void WhenICalculateGCD()
        {
            result = EuclidGcd.Calculate(value1, value2);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            Assert.AreEqual(p0, result);
        }
    }
}
