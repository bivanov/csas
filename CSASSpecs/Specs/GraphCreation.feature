﻿Feature: GraphCreation

@vertices
Scenario: Create graph with vertices
	Given I have created Graph
	And I have created serveral vertices with values 17, 34 and 18
	When I add those vertices into graph
	Then the graph's vertices should contain 3 items

@edges
Scenario: Create graph with vertices and edges
	Given I have created Graph
	And I have created and added 6 vertices into it
	When I add edge between vertex 1 and 2 with value 7
	When I add edge between vertex 1 and 3 with value 9
	When I add edge between vertex 1 and 6 with value 14
	When I add edge between vertex 2 and 3 with value 10
	When I add edge between vertex 3 and 6 with value 2
	When I add edge between vertex 2 and 4 with value 15
	When I add edge between vertex 3 and 4 with value 11
	When I add edge between vertex 4 and 5 with value 6
	When I add edge between vertex 5 and 6 with value 9
	Then the graph's edges should contain 9 items
	Then min distance between vertices 1 and 5 should be 20
	Then min distance between vertices 1 and 4 should be 20
	Then min distance between vertices 5 and 1 should be 20
	Then min distance between vertices 5 and 3 should be 11