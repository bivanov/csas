﻿Feature: SampleSort

@sort
Scenario: Sort array with sample sort
	Given I have entered data into array for sample sort:
	| Value |
	| 6     |
	| 7     |
	| 12    |
	| 10    |
	| 15    |
	| 17    |
	| 5     |
	When I sort array with sample sort
	Then array should be sorted after sample sort

@sort
Scenario: Sort array with empty data
	Given I do nothing
	When I sort array with sample sort
	Then exception should occur
