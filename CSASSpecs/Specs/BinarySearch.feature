﻿Feature: BinarySearch

@binarySearch
Scenario: Search for element in sequence
	Given I have put into 10 items; all have " " title except "BINGO!" for number 7
	When I search for item with value 7
	Then the result should have title "BINGO!"
	When I search for item with value 87
	Then the resuls should be null

@binarySearch
Scenario: Search for element in sequence by custom field
	Given I have put into 18 items; all have " " title except "BINGO!" for number 10
	When I search for item with title "BINGO!"
	Then the result should have value 10

@binarySearch
Scenario: Search for all elements that satisfy given condition
	Given I have put into 35 items; all have " " title except "BINGO!" for numbers 10 and 15 and 32
	When I search for items with title "BINGO!"
	Then the result collection should have length 3
