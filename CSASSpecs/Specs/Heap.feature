﻿Feature: Heap

@heap
Scenario: Create binary heap from data array
	Given I have entered data into array:
	| Value |
	| 6     |
	| 7     |
	| 12    |
	| 10    |
	| 15    |
	| 17    |
	| 5     |
	When I add array into heap
	Then heap should be like:
	| Value |
	| 5     |
	| 7     |
	| 6     |
	| 10    |
	| 15    |
	| 17    |
	| 12    |
	When I sort array with heap
	Then array should be sorted
	When I sort array with heap descending
	Then array should be sorted descending
