﻿Feature: LinkedList

@linkedlist
Scenario: Add items into linked list
	Given I have created linked list of int with root value 1
	And added element with value 2 as last
	And added element with value 3 after element with value 1
	When I pick element number 1 from list 
	Then it's value should be 3
	When I remove root element from list
	When I pick element number 0 from list
	Then it's value should be 3

@linkedlist
Scenario: Convert items from linked list into array
	Given I have created linked list of int with root value 1
	And added element with value 2 as last
	And added element with value 3 as last
	When I convert linked list into array
	Then resulted array should be like this:
	| Value |
	| 1     |
	| 2     |
	| 3     |

@linkedlist
Scenario: Search through linked list
	Given I have created linked list of int with root value 10
	And added element with value 20 as last
	And added element with value 30 as last
	When I search for elements with value greater than 15  
	Then resulted list should be like this:
	| Value |
	| 20    |
	| 30    |

@linkedList
Scenario: Reverse linked list
	Given I have created linked list of int with root value 10
	And added element with value 20 as last
	And added element with value 30 as last
	When I make list reversed
	Then resulted list should be like this:
	| Value |
	| 30    |
	| 20    |
	| 10    |

