﻿Feature: BubbleSort

@sort
Scenario: Sort array with bubble sort
	Given I have entered data into array for bubble sort:
	| Value |
	| 6     |
	| 7     |
	| 12    |
	| 10    |
	| 15    |
	| 17    |
	| 5     |
	When I sort array with bubble sort
	Then array should be sorted after bubble sort
