﻿Feature: EuclidianGCD

@euclid
Scenario: Find GCD for two numbers
	Given I have entered 462 and 1071
	When I calculate GCD
	Then the result should be 21

@euclid
Scenario: Find GCD for two numbers where one is aliquot to another
	Given I have entered 25 and 5
	When I calculate GCD
	Then the result should be 5
