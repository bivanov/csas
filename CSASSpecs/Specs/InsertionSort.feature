﻿Feature: InsertionSort

@sort
Scenario: Sort array with insertion sort
	Given I have entered data into array for insertion sort:
	| Value |
	| 6     |
	| 7     |
	| 12    |
	| 10    |
	| 15    |
	| 17    |
	| 5     |
	When I sort array with insertion sort
	Then array should be sorted after insertion sort
