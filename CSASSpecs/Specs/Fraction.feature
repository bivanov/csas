﻿Feature: Fraction

@fraction
Scenario: Calculate fraction with numerator and denominator
	Given I have created fraction with values 5 and 4
	Then float representation should be near 1.25
	Then int representation should be 1
	Then string representation should be '5/4'

Scenario: Calculate fraction created with float
	Given I have created fraction with float 3.6
	Then int representation should be 3
	Then string representation should be '18/5'

Scenario: Calculate fraction created with string from float
	Given I have created fraction with string '2.5'
	Then int representation should be 2
	Then string representation should be '5/2'

Scenario: Calculate fraction created with string from fraction
	Given I have created fraction with string '7/5'
	Then float representation should be near 1.4
	Then int representation should be 1

Scenario: Create fraction with same numerator and denominator
	Given I have created fraction with values 5 and 5
	Then string representation should be '1/1'

Scenario: Calculate sum of two fractions
	Given I have created one fraction with values 2 and 7
	Given I have created second fraction with values 3 and 5
	When I calculate sum of these fractions
	Then float representation should be near 0.886
	Then string representation should be '31/35'

Scenario: Calculate difference of two fractions
	Given I have created one fraction with values 4 and 7
	Given I have created second fraction with values 1 and 5
	When I calculate difference of these fractions
	Then float representation should be near 0.371
	Then string representation should be '13/35'

Scenario: Calculate negative difference of two fractions
	Given I have created one fraction with values 1 and 5
	Given I have created second fraction with values 2 and 5
	When I calculate difference of these fractions
	Then float representation should be near -0.2
	Then string representation should be '-1/5'

Scenario: Calculate product of two fractions
	Given I have created one fraction with values 1 and 6
	Given I have created second fraction with values 2 and 3
	When I calculate product of these fractions
	Then float representation should be near 0.111
	Then string representation should be '1/9'

Scenario: Calculate quotient of two fractions
	Given I have created one fraction with values 3 and 2
	Given I have created second fraction with values 3 and 2
	When I calculate quotient of these fractions
	Then float representation should be near 1.0
	Then string representation should be '1/1'

Scenario: Create fraction with improper string
	Given I have created fraction with string 'c54'
	Then exception "Exception_InvalidArgument" should occur

Scenario: Compare two fractions
	Given I have created one fraction with values 1 and 5
	Given I have created second fraction with values 1 and 6
	When I compare these fractions
	Then comparison result should be positive

Scenario: Compare two fractions 2
	Given I have created one fraction with values 1 and 6
	Given I have created second fraction with values 1 and 5
	When I compare these fractions
	Then comparison result should be negative

Scenario: Compare two fractions 3
	Given I have created one fraction with values 4 and 6
	Given I have created second fraction as copy of first
	When I compare these fractions
	Then comparison result should be 0
