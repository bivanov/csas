﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Algorithms;

namespace CSASSpecs
{
    [Binding]
    public class InsertionSortSteps
    {
        int[] data;
        int[] sortedData;

        [Given(@"I have entered data into array for insertion sort:")]
        public void GivenIHaveEnteredDataIntoArrayForInsertionSort(Table table)
        {
            data = new int[table.Rows.Count];
            int currentIndex = 0;

            foreach (TableRow value in table.Rows)
            {
                data[currentIndex++] = value.GetInt32("Value");
            }
        }

        [When(@"I sort array with insertion sort")]
        public void WhenISortArrayWithInsertionSort()
        {
            InsertionSort<int> insertionSort = new InsertionSort<int>(data);
            sortedData = insertionSort.Sort();
        }

        [Then(@"array should be sorted after insertion sort")]
        public void ThenArrayShouldBeSortedAfterInsertionSort()
        {
            Assert.NotNull(sortedData);

            for (int i = 0; i < sortedData.Length - 1; i++)
            {
                Assert.True(sortedData[i] <= sortedData[i + 1]);
            }
        }
    }
}
