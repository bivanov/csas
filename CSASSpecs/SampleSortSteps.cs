﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Algorithms;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class SampleSortSteps
    {
        int[] data;
        int[] sortedData;

        [Given(@"I have entered data into array for sample sort:")]
        public void GivenIHaveEnteredDataIntoArrayForSampleSort(Table table)
        {
            data = new int[table.Rows.Count];
            int currentIndex = 0;

            foreach (TableRow value in table.Rows)
            {
                data[currentIndex++] = value.GetInt32("Value");
            }
        }

        [When(@"I sort array with sample sort")]
        public void WhenISortArrayWithSampleSort()
        {
            Func<int, int> determiner = i =>
            {
                return i/10;
            };

            try
            {
                SampleSort<int> sampleSort = new SampleSort<int>(data, determiner);
                sortedData = sampleSort.Sort().Result;
            }
            catch (Exception e)
            {
                ScenarioContext.Current.Add("Exception_SortArrayWithSampleSort", e);
            }
        }

        [Then(@"array should be sorted after sample sort")]
        public void ThenArrayShouldBeSortedAfterSampleSort()
        {
            Assert.NotNull(sortedData);

            for (int i = 0; i < sortedData.Length - 1; i++)
            {
                Assert.True(sortedData[i] <= sortedData[i + 1]);
            }
        }

        [Given(@"I do nothing")]
        public void GivenIDoNothing()
        {

        }

        [Then(@"exception should occur")]
        public void ThenExceptionShouldOccur()
        {
            var exception = ScenarioContext.Current["Exception_SortArrayWithSampleSort"];
            Assert.That(exception, Is.Not.Null);
        }

    }
}
