﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Classes;
using Csas.Classes.Extensions;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class GraphCreationSteps
    {
        IntGraph<int> graph;
        Vertex<int> v1;
        Vertex<int> v2;
        Vertex<int> v3;

        [Given(@"I have created Graph")]
        public void GivenIHaveCreatedGraph()
        {
            graph = new IntGraph<int>();
        }

        [Given(@"I have created serveral vertices with values (.*), (.*) and (.*)")]
        public void GivenIHaveCreatedServeralVerticesWithValuesAnd(int p0, int p1, int p2)
        {
            v1 = new Vertex<int>(p0);
            v2 = new Vertex<int>(p1);
            v3 = new Vertex<int>(p2);
        }

        [When(@"I add those vertices into graph")]
        public void WhenIAddThoseVerticesIntoGraph()
        {
            graph.AddVertex(v1);
            graph.AddVertex(v2);
            graph.AddVertex(v3);
        }

        [Then(@"the graph's vertices should contain (.*) items")]
        public void ThenTheGraphSVerticesShouldContainItems(int p0)
        {
            Assert.AreEqual(graph.Vertices.Count, p0);
        }

        [Given(@"I have created and added (.*) vertices into it")]
        public void GivenIHaveCreatedAndAddedVerticesIntoIt(int p0)
        {
            graph = new IntGraph<int>();

            for (int i = 0; i < p0; i++)
            {
                Vertex<int> vertex = new Vertex<int>(i);
                vertex.VertexId = i;
                graph.AddVertex(vertex);
            }
        }

        [When(@"I add edge between vertex (.*) and (.*) with value (.*)")]
        public void WhenIAddEdgeBetweenVertexAndWithValue(int p0, int p1, int p2)
        {
            Vertex<int> vertex1 = graph.Vertices[p0 - 1];
            Vertex<int> vertex2 = graph.Vertices[p1 - 1];

            IntEdge<int> edge = new IntEdge<int>(p2, vertex1, vertex2);

            graph.AddEdge(edge);
        }

        [Then(@"the graph's edges should contain (.*) items")]
        public void ThenTheGraphSEdgesShouldContainItems(int p0)
        {
            Assert.AreEqual(graph.Edges.Count, p0);
        }

        [Then(@"vertex (.*) should be related to (.*) edges")]
        public void ThenVertexShouldBeRelatedToEdges(int p0, int p1)
        {
            Vertex<int> vertex = graph.Vertices[p0 - 1];
            Assert.AreEqual(graph.GetEdgesWithVertex(vertex).Count, p1);
        }

        [Then(@"min distance between vertices (.*) and (.*) should be (.*)")]
        public void ThenMinDistanceBetweenVerticesAndShouldBe(int p0, int p1, int p2)
        {
            Assert.AreEqual(graph.DijkstraLength<int>(graph.Vertices[p0 - 1], graph.Vertices[p1 - 1]), p2);
        }

    }
}
