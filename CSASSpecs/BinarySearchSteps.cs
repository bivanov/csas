﻿using System;
using TechTalk.SpecFlow;

using Csas.Classes;
using Csas.Algorithms;
using System.Collections.Generic;
using NUnit.Framework;
using System.Collections.ObjectModel;

namespace CSASSpecs
{
    [Binding]
    public class BinarySearchSteps
    {
        Collection<SearchObjectMock> mocks;
        Collection<SearchObjectMock> foundMocks;
        SearchObjectMock foundMock;

        [Given(@"I have put into (.*) items; all have ""(.*)"" title except ""(.*)"" for number (.*)")]
        public void GivenIHavePutIntoItemsAllHaveTitleExceptForNumber(int p0, string p1, string p2, int p3)
        {
            mocks = new Collection<SearchObjectMock>();
  
            for (int i = 0; i < p0; i++)
            {
                SearchObjectMock mock;
                if (i == p3)
                {
                    mock = new SearchObjectMock(i, p2);
                }
                else
                {
                    mock = new SearchObjectMock(i, p1);
                }

                mocks.Add(mock);
            }
        }

        [When(@"I search for item with value (.*)")]
        public void WhenISearchForItemWithValue(int p0)
        {
            BinarySearch<SearchObjectMock> search = new BinarySearch<SearchObjectMock>(mocks, false);
            foundMock = search.Search(new SearchObjectMock(p0, ""));
        }

        [Then(@"the result should have title ""(.*)""")]
        public void ThenTheResultShouldHaveTitle(string p0)
        {
            Assert.AreEqual(p0, foundMock.Title);
        }

        [Then(@"the resuls should be null")]
        public void ThenTheResulsShouldBeNull()
        {
            Assert.AreEqual(null, foundMock);
        }

        [When(@"I search for item with title ""(.*)""")]
        public void WhenISearchForItemWithTitle(string p0)
        {
            Func<SearchObjectMock, SearchObjectMock, int> sortingComparsion = (mock1, mock2) =>
                {
                    return mock1.Title.CompareTo(mock2.Title);
                };

            BinarySearch<SearchObjectMock> search = new BinarySearch<SearchObjectMock>(mocks, false, sortingComparsion);

            Func<SearchObjectMock, int> comparsion = (mock) =>
                {
                    return p0.CompareTo(mock.Title);
                };

            foundMock = search.Search(comparsion);
        }

        [Then(@"the result should have value (.*)")]
        public void ThenTheResultShouldHaveValue(int p0)
        {
            Assert.AreEqual(p0, foundMock.MockValue);
        }

        [Given(@"I have put into (.*) items; all have ""(.*)"" title except ""(.*)"" for numbers (.*) and (.*) and (.*)")]
        public void GivenIHavePutIntoItemsAllHaveTitleExceptForNumbersAndAnd(int p0, string p1, string p2, int p3, int p4, int p5)
        {
            mocks = new Collection<SearchObjectMock>();

            for (int i = 0; i < p0; i++)
            {
                SearchObjectMock mock;
                if (i == p3 || i == p4 || i == p5)
                {
                    mock = new SearchObjectMock(i, p2);
                }
                else
                {
                    mock = new SearchObjectMock(i, p1);
                }

                mocks.Add(mock);
            }
        }

        [When(@"I search for items with title ""(.*)""")]
        public void WhenISearchForItemsWithTitle(string p0)
        {
            Func<SearchObjectMock, SearchObjectMock, int> sortingComparison = (mock1, mock2) =>
            {
                return mock1.Title.CompareTo(mock2.Title);
            };

            BinarySearch<SearchObjectMock> search = new BinarySearch<SearchObjectMock>(mocks, false, sortingComparison);

            Func<SearchObjectMock, int> comparison = (mock) =>
            {
                return p0.CompareTo(mock.Title);
            };

            foundMocks = search.SearchForAll(comparison);
        }

        [Then(@"the result collection should have length (.*)")]
        public void ThenTheResultCollectionShouldHaveLength(int p0)
        {
            Assert.AreEqual(p0, foundMocks.Count);
        }


    }
}
