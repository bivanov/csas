﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSASSpecs
{
    public class SearchObjectMock : IComparable
    {
        int mockValue;
        public int MockValue
        {
            get { return mockValue; }
        }
        string title;
        public string Title
        {
            get { return title; }
        }

        public SearchObjectMock(int value, string mockTitle)
        {
            mockValue = value;
            title = mockTitle;
        }

        public int CompareTo(object anotherMock)
        {
            return (mockValue - ((SearchObjectMock)anotherMock).MockValue);
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", title, mockValue);
        }
    }
}
