﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Algorithms;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class BubbleSortSteps
    {
        int[] data;
        int[] sortedData;

        [Given(@"I have entered data into array for bubble sort:")]
        public void GivenIHaveEnteredDataIntoArrayForBubbleSort(Table table)
        {
            data = new int[table.Rows.Count];
            int currentIndex = 0;

            foreach (TableRow value in table.Rows)
            {
                data[currentIndex++] = value.GetInt32("Value");
            }
        }

        [When(@"I sort array with bubble sort")]
        public void WhenISortArrayWithBubbleSort()
        {
            BubbleSort<int> bubbleSort = new BubbleSort<int>(data);
            sortedData = bubbleSort.Sort();

            for (int i = 0; i < sortedData.Length; i++)
                Console.WriteLine("{0}: {1}", i, sortedData[i]);
        }

        [Then(@"array should be sorted after bubble sort")]
        public void ThenArrayShouldBeSortedAfterBubbleSort()
        {
            Assert.NotNull(sortedData);

            for (int i = 0; i < sortedData.Length - 1; i++)
            {
                Assert.True(sortedData[i] <= sortedData[i + 1]);
            }
        }

    }
}
