﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

using Csas.Classes;
using NUnit.Framework;

namespace CSASSpecs
{
    [Binding]
    public class LinkedListSteps
    {
        LinkedList<int> linkedList;
        LinkedListNode<int> examinedNode;
        int[] arrayFromLinkedList;
        System.Collections.Generic.List<int> searchList;

        [Given(@"I have created linked list of int with root value (.*)")]
        public void GivenIHaveCreatedLinkedListOfIntWithRootValue(int p0)
        {
            LinkedListNode<int> node0 = new LinkedListNode<int>(p0);

            linkedList = new LinkedList<int>(node0);
        }

        [Given(@"added element with value (.*) as last")]
        public void GivenAddedElementWithValueAsLast(int p0)
        {
            int length = linkedList.Length;

            LinkedListNode<int> node = new LinkedListNode<int>(p0);

            linkedList.Insert(node);

            Assert.AreEqual(length + 1, linkedList.Length);
        }

        [Given(@"added element with value (.*) after element with value (.*)")]
        public void GivenAddedElementWithValueAfterElementWithValue(int p0, int p1)
        {
            int length = linkedList.Length;

            LinkedListNode<int> nodeToInsertAfter = null;

            foreach (LinkedListNode<int> node in linkedList)
            {
                if (node.Value == p1)
                {
                    nodeToInsertAfter = node;
                    break;
                }
            }

            Assert.AreNotEqual(null, nodeToInsertAfter);

            linkedList.Insert(new LinkedListNode<int>(p0), nodeToInsertAfter);

            Assert.AreEqual(length + 1, linkedList.Length);
        }

        [When(@"I remove root element from list")]
        public void WhenIRemoveRootElementFromList()
        {
            LinkedListNode<int> firstNode = linkedList.RootNode;
            linkedList.Remove(firstNode);
        }

        [When(@"I pick element number (.*) from list")]
        public void WhenIPickElementNumberFromList(int p0)
        {
            examinedNode = linkedList.NodeForIndex(p0);
        }

        [Then(@"it's value should be (.*)")]
        public void ThenItSValueShouldBe(int p0)
        {
            Assert.AreEqual(p0, examinedNode.Value);
        }

        [When(@"I convert linked list into array")]
        public void WhenIConvertLinkedListIntoArray()
        {
            arrayFromLinkedList = linkedList.ToArray();
        }

        [Then(@"resulted array should be like this:")]
        public void ThenResultedArrayShouldBeLikeThis(Table table)
        {
            int currentIndex = 0;
            foreach (TableRow value in table.Rows)
            {
                Assert.AreEqual(value.GetInt32("Value"), arrayFromLinkedList[currentIndex]);
                currentIndex++;
            }
        }

        [When(@"I search for elements with value greater than (.*)")]
        public void WhenISearchForElementsWithValueGreaterThan(int p0)
        {
            searchList = linkedList[(value) => { return value > p0; }];
            arrayFromLinkedList = searchList.ToArray();
        }

        [Then(@"resulted list should be like this:")]
        public void ThenResultedListShouldBeLikeThis(Table table)
        {
            int currentIndex = 0;
            foreach (TableRow value in table.Rows)
            {
                Assert.AreEqual(value.GetInt32("Value"), arrayFromLinkedList[currentIndex]);
                currentIndex++;
            }
        }

        [When(@"I make list reversed")]
        public void WhenIMakeListReversed()
        {
            var reversedList = linkedList.Reverse();
            arrayFromLinkedList = reversedList.ToArray();
        }

    }
}
