﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Csas.Classes;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Collections.ObjectModel;

namespace Csas.Classes.Extensions
{
    public static class GraphExtensions
    {
        public static Collection<Edge<VT, int>> GetEdgesWithVertex<VT>(this Graph<VT, int> graph, Vertex<VT> vertex)
        {
            Contract.Requires(vertex != null);

            Collection<Edge<VT, int>> returnValue = new Collection<Edge<VT, int>>();

            foreach (IntEdge<VT> edge in graph.Edges)
            {
                if (vertex == edge.Vertex1 || vertex == edge.Vertex2)
                {
                    returnValue.Add(edge);
                }
            }

            return returnValue;
        }

        public static List<Vertex<VT>> GetVerticesRelatedToVertex<VT>(this Graph<VT, int> graph, Vertex<VT> vertex)
        {
            Contract.Requires(vertex != null);

            List<Vertex<VT>> returnValue = new List<Vertex<VT>>();

            Collection<Edge<VT, int>> edges = graph.GetEdgesWithVertex(vertex);

            foreach (Edge<VT, int> edge in edges)
            {
                Vertex<VT> vertexToReturn = edge.Vertex1 == vertex ? edge.Vertex2 : edge.Vertex1;
                returnValue.Add(vertexToReturn);
            }

            return returnValue;
        }

        public static int DijkstraLength<T>(this Graph<T, int> graph, Vertex<T> startingVertex, Vertex<T> endingVertex)
        {
            Contract.Assert(startingVertex != null && endingVertex != null);

            List<Vertex<T>> graphVertices = new List<Vertex<T>>(graph.Vertices);

            int returnValue = -1;

            Dictionary<Vertex<T>, int> valuesDictionary = new Dictionary<Vertex<T>, int>();

            foreach (Vertex<T> vertex in graphVertices)
            {
                valuesDictionary[vertex] = vertex == startingVertex ? 0 : int.MaxValue - 1;
            }

            do
            {
                graphVertices = graphVertices.OrderBy((vertex) => valuesDictionary[vertex]).ToList();

                Vertex<T> currentVertex = graphVertices[0];

                Collection<Edge<T, int>> relatedEdges = graph.GetEdgesWithVertex(currentVertex);

                foreach (Edge<T, int> edge in relatedEdges)
                {
                    Vertex<T> anotherVertex = edge.Vertex1 == currentVertex ? edge.Vertex2 : edge.Vertex1;

                    int pathValue = valuesDictionary[currentVertex] + edge.Value;

                    if (pathValue < valuesDictionary[anotherVertex] ||
                        valuesDictionary[anotherVertex] < 0)
                    {
                        valuesDictionary[anotherVertex] = pathValue;
                    }
                }

                graphVertices.Remove(currentVertex);
            }
            while (graphVertices.Count > 0);

            returnValue = valuesDictionary[endingVertex];

            return returnValue;
        }
    }
}
