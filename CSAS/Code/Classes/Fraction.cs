﻿using System;
using System.Data.SqlTypes;
using Csas.Algorithms;


namespace Csas.Classes
{
    /// <summary>
    /// Provides implementation for simple fraction
    /// with basic arithmethic operations.
    /// For alternative you could look at http://www.codeproject.com/Articles/9078/Fraction-class-in-C
    /// </summary>
    public class Fraction : IComparable
    {
        /// <summary>
        /// Numerator of fraction
        /// </summary>
        public int Numerator { get; private set; }
        /// <summary>
        /// Denominator of fraction
        /// </summary>
        public int Denominator { get; private set; }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="numerator">Numerator of fraction</param>
        /// <param name="denominator">Denominator of fraction</param>
        /// <exception cref="ArgumentException"></exception>
        public Fraction(int numerator, int denominator)
        {
            if (denominator <= 0)
            {
                throw new ArgumentException("Denominator should be greater then zero"); 
            }

            Numerator = numerator;
            Denominator = denominator;

            Normalize();
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="f">Float value that should be converted
        /// into fraction
        /// </param>
        public Fraction(float f)
        {
            Denominator = 1;

            // We can ignore precision loss here
            while (f != Math.Floor(f))
            {
                f *= 10;
                Denominator *= 10;
            }

            Numerator = (int) f;

            Normalize();
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="d">Float value that should be converted
        /// into fraction
        /// </param>
        public Fraction(double d)
        {
            Denominator = 1;

            // We can ignore precision loss here
            while (d != Math.Floor(d))
            {
                d *= 10;
                Denominator *= 10;
            }

            Numerator = (int)d;

            Normalize();
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="s">String that could be converted to fraction.
        /// E.g. "1.66" or "3/4"</param>
        /// <exception cref="ArgumentException"></exception>
        public Fraction(string s)
        {
            Denominator = 1;
            double d;

            try
            {
                d = Convert.ToDouble(s);

                // We can ignore precision loss here
                while (d != Math.Floor(d))
                {
                    d *= 10;
                    Denominator *= 10;
                }

                Numerator = (int)d;

                Normalize();
            }
            catch (Exception)
            {
                try
                {
                    char[] separator = {'/'};
                    var values = s.Split(separator);
                    Numerator = Convert.ToInt32(values[0]);
                    Denominator = Convert.ToInt32(values[1]);

                    Normalize();
                }
                catch (Exception)
                {
                    throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="fractionFloat">Fraction that another one should be copied from</param>
        public Fraction(Fraction fractionFloat)
        {
            Numerator = fractionFloat.Numerator;
            Denominator = fractionFloat.Denominator;
        }

        /// <summary>
        /// Sum operator
        /// </summary>
        /// <param name="fractionFloat1">Fisrt operand</param>
        /// <param name="fractionFloat2">Second operand</param>
        /// <returns>Sum of operands</returns>
        public static Fraction operator +(
            Fraction fractionFloat1, Fraction fractionFloat2)
        {
            int gcd = EuclidGcd.Calculate(fractionFloat1.Denominator, fractionFloat2.Denominator);
            int lcmultiple = fractionFloat1.Denominator * fractionFloat2.Denominator / gcd;

            int a = lcmultiple / fractionFloat1.Denominator;
            int b = lcmultiple / fractionFloat2.Denominator;

            return new Fraction(fractionFloat1.Numerator * a + fractionFloat2.Numerator * b, lcmultiple);
        }

        /// <summary>
        /// Substraction operator
        /// </summary>
        /// <param name="fractionFloat1">First operand</param>
        /// <param name="fractionFloat2">Second operand</param>
        /// <returns>Difference of operands</returns>
        public static Fraction operator -(
            Fraction fractionFloat1, Fraction fractionFloat2)
        {
            int gcd = EuclidGcd.Calculate(fractionFloat1.Denominator, 
                fractionFloat2.Denominator);
            int lcmultiple = fractionFloat1.Denominator * fractionFloat2.Denominator / gcd;

            int a = lcmultiple / fractionFloat1.Denominator;
            int b = lcmultiple / fractionFloat2.Denominator;

            return new Fraction(fractionFloat1.Numerator * a - fractionFloat2.Numerator * b, 
                lcmultiple);
        }

        /// <summary>
        /// Mul operator
        /// </summary>
        /// <param name="fractionFloat1">First operand</param>
        /// <param name="fractionFloat2">Second operand</param>
        /// <returns>Product of two operands</returns>
        public static Fraction operator *(
            Fraction fractionFloat1, Fraction fractionFloat2)
        {
            return new Fraction(fractionFloat1.Numerator * fractionFloat2.Numerator,
                fractionFloat1.Denominator * fractionFloat2.Denominator);
        }

        /// <summary>
        /// Division operator
        /// </summary>
        /// <param name="fractionFloat1">First operand</param>
        /// <param name="fractionFloat2">Second operand</param>
        /// <returns>Quotient of division of first operand by second one</returns>
        public static Fraction operator /(
            Fraction fractionFloat1, Fraction fractionFloat2)
        {
            return new Fraction(fractionFloat1.Numerator * fractionFloat2.Denominator,
                fractionFloat1.Denominator * fractionFloat2.Numerator);
        }

        public static explicit operator float(Fraction fractionFloat)
        {
            return fractionFloat.Numerator / (float)fractionFloat.Denominator;
        }

        public static explicit operator int(Fraction fractionFloat)
        {
            return fractionFloat.Numerator / fractionFloat.Denominator;
        }

        public static explicit operator Fraction(float f)
        {
            return new Fraction(f);
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", Numerator, Denominator);
        }

        private void Normalize()
        {
            if (Numerator == Denominator)
            {
                Numerator = Denominator = 1;
                return;
            }

            if (Numerator == 0)
            {
                return;
            }

            int gcd = EuclidGcd.Calculate(Math.Abs(Numerator), Denominator);

            Numerator /= gcd;
            Denominator /= gcd;
        }

        public int CompareTo(object obj)
        {
            return (this - (Fraction) obj).Numerator;
        }
    }
}
