﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Classes
{
    public class Edge<VT, ET>
    {
        public ET Value { get; set; }
        public Vertex<VT> Vertex1 { get; private set; }
        public Vertex<VT> Vertex2 { get; private set; }

        public Edge()
        {

        }

        public Edge(ET value, Vertex<VT> vertex1, Vertex<VT> vertex2)
        {
            Value = value;
            Vertex1 = vertex1;
            Vertex2 = vertex2;
        }

        public Vertex<VT> GetAnotherVertex(Vertex<VT> vertex)
        {
            Contract.Assume(vertex == Vertex1 || vertex == Vertex2);

            return vertex == Vertex1 ? Vertex2 : Vertex1;
        }
    }

    /// <summary>
    /// Edge with int as edge value
    /// \deprecated since 1.0.13. Use Edge<VT, int> instead
    /// </summary>
    /// <typeparam name="VT">Value of edge</typeparam>
    public class IntEdge<VT> : Edge<VT, int>
    {
        public IntEdge(int value, Vertex<VT> vertex1, Vertex<VT> vertex2) 
            : base(value, vertex1, vertex2)
        {

        }
    }
}
