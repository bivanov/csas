﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Csas.Classes
{
    public interface ILinkedList<T>
    {
        void Insert(LinkedListNode<T> node);
        void Remove(LinkedListNode<T> node);
        List<T> Search(Func<T, bool> searchFunction);
        ILinkedList<T> Reverse();
    }

    public class LinkedListNode<T>
    {
        public LinkedListNode<T> PreviousNode { get; set; }
        public LinkedListNode<T> NextNode { get; set; }
        T nodeValue;
        public T Value
        {
            get
            {
                return nodeValue;
            }
        }

        public LinkedListNode(T val)
        {
            NextNode = null;
            nodeValue = val;
        }

        public LinkedListNode(LinkedListNode<T> node)
        {
            NextNode = node.NextNode;
            nodeValue = node.Value;
        }

        public override string ToString()
        {
            return string.Format("[LockFreeLinkedListNode: Value={0}]", nodeValue);
        }
    }

    /// <summary>
    /// Lock-free (or at least believed to be) realization of linked list 
    /// data structure
    /// </summary>
    /// <typeparam name="T">Type of items that linked list operates with</typeparam>
    public class LinkedList<T> : IEnumerable<LinkedListNode<T>>
    {
        LinkedListNode<T> _rootNode;
        LinkedListNode<T> _lastNode;
        int length = 0;

        /// <summary>
        /// First element of linked list
        /// </summary>
        public LinkedListNode<T> RootNode
        {
            get { return _rootNode; }
        }

        /// <summary>
        /// Count of linked list elements
        /// </summary>
        public int Length
        {
            get { return length; }
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="node">Root node of linked list. Should not be null</param>
        public LinkedList(LinkedListNode<T> node)
        {
            _rootNode = node;
            _lastNode = node;
            length += 1;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="list">Linked list that should be copied into new instance</param>
        public LinkedList(LinkedList<T> list)
        {
            _rootNode = list.RootNode;
            _lastNode = list._lastNode;
            length = list.Length;
        }

        /// <summary>
        /// Adds node item to a linked list after last element
        /// </summary>
        /// <param name="nodeToInsert">Node that should be added</param>
        public void Insert(LinkedListNode<T> nodeToInsert)
        {
            _lastNode.NextNode = nodeToInsert;
           // nodeToInsert.PreviousNode = lastNode;
            Interlocked.Exchange(ref _lastNode, nodeToInsert);
            length += 1;
        }

        /// <summary>
        /// Adds node item to a linked list after specified element
        /// </summary>
        /// <param name="nodeToInsert">Node that should be added</param>
        /// <param name="nodeToInsertAfter">Node after which one another node
        /// should be added</param>
        public void Insert(LinkedListNode<T> nodeToInsert, LinkedListNode<T> nodeToInsertAfter)
        {
            nodeToInsert.NextNode = nodeToInsertAfter.NextNode;
            nodeToInsertAfter.NextNode = nodeToInsert;

            length += 1;
        }

        /// <summary>
        /// Removes specified item from linked list
        /// </summary>
        /// <param name="nodeToRemove">Item that should be removed</param>
        public void Remove(LinkedListNode<T> nodeToRemove)
        {
            LinkedListNode<T> currentNodeToSearch = _rootNode;

            if (currentNodeToSearch == nodeToRemove)
            {
                _rootNode = _rootNode.NextNode;
            }

            else
            {
                while (currentNodeToSearch.NextNode != null)
                {
                    if (currentNodeToSearch.NextNode == nodeToRemove)
                    {
                        currentNodeToSearch.NextNode = nodeToRemove.NextNode;
                        break;
                    }

                    Interlocked.CompareExchange(ref currentNodeToSearch, currentNodeToSearch.NextNode, currentNodeToSearch);
                }
            }

            length -= 1;
        }

        /// <summary>
        /// Returns item from linked list at specified position
        /// </summary>
        /// <param name="index">Position of needed item. Should be lesser than
        /// list length</param>
        /// <returns>Found item</returns>
        public LinkedListNode<T> NodeForIndex(int index)
        {
            int currentIndex = -1;

            LinkedListNode<T> currentNodeToSearch = _rootNode;

            do
            {
                if (++currentIndex == index)
                {
                    break;
                }
            }
            while (Interlocked.CompareExchange(ref currentNodeToSearch, currentNodeToSearch.NextNode, currentNodeToSearch) != null);

            return currentNodeToSearch;
        }

        public override string ToString()
        {
            LinkedListNode<T> currentNode = _rootNode;

            string retVal = string.Empty;

            while (currentNode != null)
            {
                retVal += currentNode.ToString();
                currentNode = currentNode.NextNode;
            }

            return retVal;
        }

        public T[] ToArray()
        {
            T[] retVal = new T[Length];
            int currentIndex = 0;

            foreach (var node in this)
            {
                retVal[currentIndex++] = node.Value;
            }

            return retVal;
        }

        /// <summary>
        /// \attention Not efficient
        /// </summary>
        /// <param name="searchFunction"></param>
        /// <returns></returns>
        public List<T> this[Func<T, bool> searchFunction]
        {
            get
            {
                return Search(searchFunction);
            }
        }

        public LinkedList<T> Reverse()
        {
            LinkedList<T> reversedList = new LinkedList<T>(new LinkedListNode<T>(_lastNode));

            var reversedListRootNode = reversedList.RootNode;
            var currentNode = _rootNode;

            while (currentNode.NextNode != null)
            {
                reversedList.Insert(new LinkedListNode<T>(currentNode), reversedListRootNode);
                currentNode = currentNode.NextNode;
            }

            return reversedList;
        }

        private List<T> Search(Func<T, bool> searchFunction)
        {
            List<T> resultedList = new List<T>(Length);
            
            foreach (var node in this)
            {
                if (searchFunction(node.Value))
                {
                    resultedList.Add(node.Value);
                }
            }

            return resultedList;
        }

        public IEnumerator<LinkedListNode<T>> GetEnumerator()
        {
            return GetCustomEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        LinkedListEnumerator<T, LinkedListNode<T>> GetCustomEnumerator()
        {
            return new LinkedListEnumerator<T, LinkedListNode<T>>(this);
        }
    }

    public class LinkedListEnumerator<T, NT> : IEnumerator<NT> where NT: LinkedListNode<T>
    {
        private LinkedList<T> linkedList;
        private LinkedListNode<T> _currentNode;

        public LinkedListEnumerator(LinkedList<T> list)
        {
            linkedList = list;
            _currentNode = null;
        }

        NT IEnumerator<NT>.Current
        {
            get
            {
                return (NT)_currentNode;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return _currentNode;
            } 
        }

        public NT Current
        {
            get
            {
                return (NT)_currentNode;
            }
        }

        bool IEnumerator.MoveNext()
        {
            if (_currentNode == null)
            {
                _currentNode = linkedList.RootNode;
            }
            else
            {
                _currentNode = _currentNode.NextNode;
            }
           
            return (_currentNode != null);
        }

        void IEnumerator.Reset()
        {
            _currentNode = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool value)
        {
        }
    }
}
