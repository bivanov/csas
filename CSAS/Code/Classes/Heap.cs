﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Classes
{
    [Serializable]
    public enum HeapOrder
    {
        Ascending = 0,
        Descending
    };

    public class BinaryHeap<T> where T: IComparable
    {
        /// <summary>
        /// Size of heap
        /// </summary>
        public int Size { get; private set; }
        private int currentSize = 0;
        private Func<T, T, bool> comparsionMethod;

        /// <summary>
        /// Root of heap; minimum or maximum element
        /// based on order specified in constructor
        /// </summary>
        public T Root
        {
            get
            {
                return vertices[0];
            }
        }

        private T[] vertices;
        /// <summary>
        /// Read-only collection consisted from heap data
        /// vertices
        /// </summary>
        public ReadOnlyCollection<T> Vertices
        {
            get
            {
                return Array.AsReadOnly(vertices);
            }
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="data">Array of data to insert into heap</param>
        /// <param name="order">Order of heap</param>
        /// <param name="comparison">Comparison method for heap data type</param>
        public BinaryHeap(T[] data, HeapOrder order, Func<T, T, int> comparison = null)
        {
            SetComparisonMethod(order, comparison);

            Heapify(data);
        }

        private void SetComparisonMethod(HeapOrder order, Func<T, T, int> comparison = null)
        {
            if (order == HeapOrder.Ascending)
            {
                if (comparison == null)
                {
                    comparsionMethod = ((item1, item2) => { return item1.CompareTo(item2) > 0; });
                }
                else
                {
                    comparsionMethod = ((item1, item2) => { return comparison(item1, item2) > 0; });
                }
            }
            else
            {
                if (comparison == null)
                {
                    comparsionMethod = ((item1, item2) => { return item1.CompareTo(item2) < 0; });
                }
                else
                {
                    comparsionMethod = ((item1, item2) => { return comparison(item1, item2) > 0; });
                }
            }
        }

        private void Heapify(T[] data)
        {
            Size = data.Length;

            if (vertices == null)
            {
                vertices = new T[Size];
            }

            for (int i = 0; i < data.Length; i++)
            {
                Insert(data[i]);
            }
        }

        /// <summary>
        /// Adds element into heap
        /// </summary>
        /// <param name="vertex">Object to be added</param>
        public void Insert(T vertex)
        {
            int currentPos = currentSize++;

            vertices[currentPos] = vertex;

            while (currentPos > 0)
            {
                int parentPos = (currentPos - 1) / 2;
                if (comparsionMethod(vertices[parentPos], vertex))
                {
                    var val = vertices[parentPos];
                    vertices[parentPos] = vertices[currentPos];
                    vertices[currentPos] = val;
                    currentPos = parentPos;
                }
                else
                {
                    break;
                }
            }
        }

        private void Restore(int index)
        {
            var value = vertices[index];

            int leftChildIndex = 1 + 2 * index;
            int rightChildIndex = 2 * (index + 1);

            if (leftChildIndex >= Size && rightChildIndex >= Size) return;

            var leftChild = GetLeftChild(index);
            var rightChild = GetRightChild(index);

            if (comparsionMethod(value, leftChild) && comparsionMethod(value, rightChild))
            {
                if (!comparsionMethod(leftChild, rightChild) && leftChildIndex < Size)
                {
                    vertices[index] = leftChild;
                    vertices[leftChildIndex] = value;
                    Restore(leftChildIndex);
                }
                else if (rightChildIndex < Size)
                {
                    vertices[index] = rightChild;
                    vertices[rightChildIndex] = value;
                    Restore(rightChildIndex);
                }
            }
            else if (comparsionMethod(value, leftChild) && leftChildIndex < Size)
            {
                vertices[index] = leftChild;
                vertices[leftChildIndex] = value;
                Restore(leftChildIndex);
            }
            else if (comparsionMethod(value, rightChild) && rightChildIndex < Size)
            {
                vertices[index] = rightChild;
                vertices[rightChildIndex] = value;
                Restore(rightChildIndex);
            } 
        }

        // For next two methods we assume
        // that position is less than size of heap
        // and do not make any additional checks
        private T GetLeftChild(int position)
        {
            return vertices[1 + 2*position];
        }

        private T GetRightChild(int position)
        {
            return vertices[2 * (position + 1)];
        }

        /// <summary>
        /// Extracts root element from heap
        /// </summary>
        /// <returns>Former root of the heap</returns>
        public T ExtractRoot()
        {
            var rootValue = Root;

            vertices[0] = vertices[Size - 1];
            currentSize = 0;
            Size -= 1;

            Restore(0);

            return rootValue;
        }
    }
}
