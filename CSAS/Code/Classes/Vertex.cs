﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Classes
{
    public class Vertex<T>
    {
        public int VertexId { get; set; }
        public T Value { get; private set; }

        public Vertex(T value)
        {
            Value = value;
        }
    }
}
