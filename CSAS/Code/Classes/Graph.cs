﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;

namespace Csas.Classes
{
    public class Graph<VT, ET> where ET: IComparable
    {
        // TODO Change from list to arrays
        public List<Edge<VT, ET>> Edges { get; private set; }
        public List<Vertex<VT>> Vertices { get; private set; }
        
        public Graph()
        {
            Edges = new List<Edge<VT, ET>>();
            Vertices = new List<Vertex<VT>>();
        }

        public void AddEdge(Edge<VT, ET> edge)
        {
            Contract.Assert(!Edges.Contains(edge));

            Edges.Add(edge);
        }

        public void AddVertex(Vertex<VT> vertex)
        {
            Contract.Assert(!Vertices.Contains(vertex));

            Vertices.Add(vertex);
        }
    }

    public class IntGraph<VT> : Graph<VT, int>
    {

    }
}
