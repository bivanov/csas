﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Classes
{
    public class Either<T>
    {
        Func<T, bool> validationFunction;
        T tValue = default(T);
        public T Value
        {
            get { return tValue; }
            set { tValue = value; }
        }

        public static implicit operator Either<T>(T value)
        {
            Either<T> result = new Either<T>(value);
            result.tValue = value;
            return result;
        }

        public static explicit operator T(Either<T> either)
        {
            if (!either.validationFunction(either.tValue))
            {
                throw new InvalidOperationException();
            }

            return either.tValue;
        }

        public static implicit operator Either<T>(Func<T, bool> validation)
        {
            Either<T> result = new Either<T>();
            result.validationFunction = validation;
            return result;
        }

        private Either()
        {
            validationFunction = ((val) => { return val != null; });
        }

        private Either(T value)
        {
            Value = value;
            validationFunction = ((val) => { return val != null; });
        }
    } 

}
