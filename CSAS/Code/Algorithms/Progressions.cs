﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Algorithms
{
    public static class Progressions
    {
        public static float GetAriphmethicalProgressionMember(int memberNumber, float firstElement, float interval)
        {
            return firstElement + interval * (memberNumber - 1);
        }
    }
}
