﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Csas.Classes;
using System.Diagnostics.Contracts;

namespace Csas.Algorithms
{
    /// <summary>
    /// Provides sorting based on heap sort
    /// </summary>
    /// <typeparam name="T">Type of items that should be sorted</typeparam>
    public class HeapSort<T> where T: IComparable
    {
        T[] dataArray;

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="data">Data to be sorted</param>
        public HeapSort(T[] data)
        {
            dataArray = data;
        }

        /// <summary>
        /// Sorts data and returns resulted sorted array
        /// </summary>
        /// <param name="sortOrder">Sorting order; can be ascending or descending</param>
        /// <returns>Sorted array</returns>
        public T[] Sort(HeapOrder sortOrder = HeapOrder.Ascending)
        {
            return Sort(null, sortOrder);
        }

        /// <summary>
        /// Sorts data and returns resulted sorted array
        /// </summary>
        /// <param name="comparisonMethod">Method that will be used as comparison for two elements</param>
        /// <param name="sortOrder">Sorting order; can be ascending or descending</param>
        /// <returns>Sorted array</returns>
        /// \throws ArgumentNullException If comparisonMethod is null
        public T[] Sort(Func<T, T, int> comparisonMethod, HeapOrder sortOrder = HeapOrder.Ascending)
        {
            BinaryHeap<T> heap = new BinaryHeap<T>(dataArray, sortOrder, comparisonMethod);

            int currentIndex = 0;

            while (heap.Size > 0)
            {
                dataArray[currentIndex++] = heap.ExtractRoot();
            }

            return dataArray;
        }
    }
}
