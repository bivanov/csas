﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Csas.Algorithms;
using Csas.Classes;

namespace Csas.Algorithms
{
    /// <summary>
    /// Provides sorting based on sample sort:
    /// http://en.wikipedia.org/wiki/Samplesort
    /// </summary>
    /// <typeparam name="T">Type of items that will be sorted</typeparam>
    public class SampleSort<T> where T : IComparable
    {
        //T[,] buckets;
        private List<T>[] buckets;

        private int bucketCount = 10;
        private int dataLength;

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="unsortedData">Data to be sorted</param>
        /// <param name="indexDeterminerFunc">Function that determines index of bucket 
        /// in which one particular item should go</param>
        /// \throws ArgumentNullException If unsortedData or indexDeterminerFunc is null
        public SampleSort(T[] unsortedData, Func<T, int> indexDeterminerFunc)
        {
            if (unsortedData == null || indexDeterminerFunc == null)
            {
                throw new ArgumentNullException();
            }

            buckets = new List<T>[bucketCount];
            dataLength = unsortedData.Length;

            for (int i = 0; i < unsortedData.Length; i++)
            {
                int bucketIndex = indexDeterminerFunc(unsortedData[i]);
                List<T> bucket = buckets[bucketIndex];
                if (bucket == null)
                {
                    bucket = new List<T>();
                    buckets[bucketIndex] = bucket;
                }

                bucket.Add(unsortedData[i]);
            }
        }

        /// <summary>
        /// Sorts data and returns resulted sorted array
        /// </summary>
        /// <returns>Sorted array</returns>
        /// <param name="comparisonMethod">Method that will be used as comparisonMethod for two elements</param>
        /// <param name="sortOrder">Sorting order; can be ascending or descending</param>
        public async Task<T[]> Sort(Func<T, T, int> comparisonMethod = null, HeapOrder sortOrder = HeapOrder.Ascending)
        {
            // TODO Remove async feature
            var tasks = new List<Task<int>>();
            for (int i = 0; i < bucketCount; i++)
            {
                tasks.Add(SortBucket(i, comparisonMethod, sortOrder));
            }

            await Task.WhenAll(tasks);

            return MergeBuckets();
        }

        private async Task<int> SortBucket(int index, Func<T, T, int> comparisonMethod = null, HeapOrder sortOrder = HeapOrder.Ascending)
        {
            await Task.Run(() =>
            {
                if (buckets[index] != null)
                {
                    HeapSort<T> heapSort = new HeapSort<T>(buckets[index].ToArray());
                    buckets[index] = new List<T>(heapSort.Sort(comparisonMethod, sortOrder));
                }
            });
            return 0;
        }

        private T[] MergeBuckets()
        {
            T[] resultedArray = new T[dataLength];
            int currentIndex = 0;

            foreach (var bucket in buckets)
            {
                if (bucket == null) continue;

                foreach (T value in bucket)
                {
                     resultedArray[currentIndex++] = value;
                }  
            }

            return resultedArray;
        }
    }
}
