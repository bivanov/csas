﻿using System;
using System.Collections.Generic;
using System.Linq;

using Csas.Classes;
using Csas.Algorithms;
using System.Collections.ObjectModel;

namespace Csas.Algorithms
{
    /// \file

    /// <summary>
    /// Performs binary search on data array
    /// </summary>
    /// <typeparam name="TItem">Type of item that should be searched</typeparam>
    public class BinarySearch<TItem> where TItem: IComparable
    {
        TItem[] data;

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="items">Items search will be performed within</param>
        /// <param name="alreadySorted">Specifies if incoming data was already sorted</param>
        /// <param name="comparison">Comparison method for items sorting</param>
        /// \throws ArgumentNullException If items is null
        public BinarySearch(IEnumerable<TItem> items, bool alreadySorted, Func<TItem, TItem, int> comparison = null)
        {
            if (items == null)
            {
                throw new ArgumentNullException();
            }

            data = items.ToArray();

            if (!alreadySorted)
            {
                HeapSort<TItem> heapSort = new HeapSort<TItem>(data);

                TItem[] sortedItems = heapSort.Sort(comparison);

                int counter = 0;
                foreach (TItem item in sortedItems)
                {
                    data[counter++] = item;
                }
            }
        }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="linkedList">Linked list with data search will be performed within</param>
        /// <param name="alreadySorted">Specifies if incoming data was already sorted</param>
        /// <param name="comparison">Comparison method for items sorting</param>
        /// \throws ArgumentNullException If linkedList is null
        public BinarySearch(Csas.Classes.LinkedList<TItem> linkedList, bool alreadySorted, Func<TItem, TItem, int> comparison = null)
        {
            if (linkedList == null)
            {
                throw new ArgumentNullException();
            }

            data = linkedList.ToArray();

            if (!alreadySorted)
            {
                HeapSort<TItem> heapSort = new HeapSort<TItem>(data);

                TItem[] sortedItems = heapSort.Sort(comparison);

                int counter = 0;
                foreach (var item in sortedItems)
                {
                    data[counter++] = item;
                }
            }
        }

        /// <summary>
        /// Searches in data array for object that satisfies specified
        /// critetia
        /// </summary>
        /// <param name="itemToFind">Item that used as reference while searching.</param>
        /// <returns>Found item; null in case no item 
        /// that satisfies critetia was found</returns>
        /// \throws ArgumentNullException If itemToFind is null
        /// Reference should have comparison field initialized:
        /// \snippet Snippets/BinarySearchSnippet.cs Creating binary search with custom item
        public TItem Search(TItem itemToFind)
        {
            if (itemToFind == null)
            {
                throw new ArgumentNullException();
            }

            return Search(itemToFind, data);
        }

        /// <summary>
        /// Searches in data array for object that satisfies specified
        /// critetia
        /// </summary>
        /// <param name="comparison">Comparison method</param>
        /// <returns>Found item; null in case no item 
        /// that satisfies critetia was found
        /// </returns>
        /// \throws ArgumentNullException If comparison is null
        /// If you need to perform search with custom critetia, please ensure
        /// that your data is sorted by same field(s) you want to search with:
        /// \snippet Snippets/BinarySearchSnippet.cs Creating binary search with custom condition
        public TItem Search(Func<TItem, int> comparison)
        {
            if (comparison == null)
            {
                throw new ArgumentNullException();
            }

            int index = -1;
            return SearchWithinBoundaries(comparison, data, 0, data.Length - 1, ref index);
        }

        /// <summary>
        /// Searches in data array for objects that satisfy specified
        /// criteria
        /// </summary>
        /// <param name="comparison">Comparison method</param>
        /// <returns>Collection of found items; collection may be empty
        /// if any item was found
        /// </returns>
        /// \throws ArgumentNullException If comparison is null
        /// In general you should obey similar rules as for Search(Func<TItem, int> comparison) method
        public Collection<TItem> SearchForAll(Func<TItem, int> comparison)
        {
            if (comparison == null)
            {
                throw new ArgumentNullException();
            }

            return SearchForAll(comparison, data);
        }

        private Collection<TItem> SearchForAll(Func<TItem, int> comparison, TItem[] dataArray)
        {
            Collection<TItem> foundItems = new Collection<TItem>();

            int index = -1;

            TItem firstFoundItem = SearchWithinBoundaries(comparison, dataArray, 0, dataArray.Length - 1, ref index);

            if (firstFoundItem != null && index >= 0)
            {
                foundItems.Add(firstFoundItem);
                int dataLength = dataArray.Length;

                int currentIndex = index + 1;

                if (currentIndex < dataLength)
                {
                    while (comparison(dataArray[currentIndex]) == 0)
                    {
                        foundItems.Add(dataArray[currentIndex++]);

                        if (currentIndex >= dataLength) break;
                    }
                }

                currentIndex = index - 1;

                if (currentIndex >= 0)
                {
                    while (comparison(dataArray[currentIndex]) == 0)
                    {
                        foundItems.Add(dataArray[currentIndex--]);

                        if (currentIndex < 0) break;
                    }
                }
            }

            return foundItems;
        }

        private TItem Search(TItem itemToFind, TItem[] dataArray)
        {
            if (dataArray.Length == 0)
            {
                return default(TItem);
            }

            int medianIndex = dataArray.Length / 2;

            int comparsionResult = itemToFind.CompareTo(dataArray[medianIndex]);

            if (comparsionResult == 0)
            {
                return dataArray[medianIndex];
            }
            else if (medianIndex == 0)
            {
                return default(TItem);
            }
            else if (comparsionResult < 0)
            {
                dataArray = dataArray.Take(medianIndex).ToArray();
                return Search(itemToFind, dataArray);
            }
            else
            {
                dataArray = dataArray.Skip(medianIndex + 1).ToArray();
                return Search(itemToFind, dataArray);
            }
        }

        private TItem SearchWithinBoundaries(Func<TItem, int> comparison, TItem[] dataArray,
            int lowerBoundary, int upperBoundary, ref int foundItemIndex)
        {
            if (dataArray.Length == 0)
            {
                return default(TItem);
            }

            int medianIndex = (upperBoundary - lowerBoundary + 1) / 2;

            int comparisonResult = comparison(dataArray[medianIndex + lowerBoundary]);

            if (comparisonResult == 0)
            {
                foundItemIndex = medianIndex + lowerBoundary;
                return dataArray[medianIndex + lowerBoundary];
            }

            if (medianIndex == 0)
            {
                return default(TItem);
            }

            if (comparisonResult < 0)
            {
                return SearchWithinBoundaries(comparison, dataArray, lowerBoundary, 
                    medianIndex + lowerBoundary, ref foundItemIndex);
            }
            
            return SearchWithinBoundaries(comparison, dataArray, medianIndex + lowerBoundary + 1, 
                upperBoundary, ref foundItemIndex);
        }   
    }
}
