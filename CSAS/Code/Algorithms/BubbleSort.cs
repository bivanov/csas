﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Csas.Algorithms
{
    /// <summary>
    /// Bubble sort probably is least efficient
    /// sorting algorithm, but it can be useful 
    /// with small amount of data.
    /// Realization is provided for benchmark
    /// purposes.
    /// </summary>
    public class BubbleSort<T> where T: IComparable
    {
        private T[] data;

        public BubbleSort(T[] unsortedData)
        {
            data = unsortedData;
        }

        public T[] Sort()
        {
            int dataSize = data.Length;

            for (int i = 0; i < dataSize; i++)
            {
                for (int j = 0; j < dataSize - 1; j++)
                {
                    if (data[j].CompareTo(data[j + 1]) > 0)
                    {
                        T tempValue = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = tempValue;
                    }
                }
            }

            return data;
        }
    }
}
