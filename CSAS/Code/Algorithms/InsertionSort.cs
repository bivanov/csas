﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Algorithms
{
    public class InsertionSort<T> where T: IComparable
    {
        T[] data;
        T[] sortedData;
        int currentSortedDataSize = 0;

        public InsertionSort(T[] unsortedData)
        {
            data = unsortedData;
            sortedData = new T[data.Length];
        }

        public T[] Sort()
        {
            int dataLength = data.Length;

            for (int i = 0; i < dataLength; i++)
            {
                InsertIntoSorted(data[i]);
            }

            return sortedData;
        }

        private void InsertIntoSorted(T item)
        {
            if (currentSortedDataSize++ == 0)
            {
                sortedData[0] = item;
                return;
            }

            sortedData[currentSortedDataSize - 1] = item;
            int currentIndex = currentSortedDataSize - 2;
            T currentItem = sortedData[currentIndex];

            while (item.CompareTo(currentItem) < 0)
            {
                sortedData[currentIndex + 1] = currentItem;
                sortedData[currentIndex] = item;

                if (currentIndex > 0)
                {
                    currentItem = sortedData[--currentIndex];
                }
                else
                {
                    break;
                }
            }
        }
    }
}
