﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas.Algorithms
{
    /// <summary>
    /// Finds greatest common divider for two decimal values
    /// </summary>
    public static class EuclidGcd
    {
        /// <summary>
        /// Finds greatest common divider for two decimal values
        /// </summary>
        /// <param name="firstValue">First value</param>
        /// <param name="secondValue">Second value</param>
        /// <returns>GCD for first and second values</returns>
        public static int Calculate(int firstValue, int secondValue)
        {
            if (secondValue > firstValue)
            {
                int intermediateValue = secondValue;
                secondValue = firstValue;
                firstValue = intermediateValue;
            }

            while (firstValue >= secondValue)
            {
                firstValue -= secondValue;
            }

            if (firstValue == 0)
            {
                return secondValue;
            }
            else
            {
                return Calculate(secondValue, firstValue);
            }
        }
    }
}
