﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Csas.Classes;
using Csas.Algorithms;
using System.Collections.ObjectModel;

namespace Csas.Snippets
{
    internal class BinarySearchSnippet
    {
        void Method2()
        {
            // [Creating binary search with custom item]

            //Collection<SearchObjectMock> mocks = new Collection<SearchObjectMock>();

            ///* SearchObjectMock has following CompareTo method:
            // public int CompareTo(object anotherMock)
            // {
            //     return (mockValue - ((SearchObjectMock)anotherMock).MockValue);
            // }
            //*/

            ///* mockObject.MockValue will be set to 15*/
            //var mockObject = new SearchObjectMock(15, "");

            //BinarySearch<SearchObjectMock> search = new BinarySearch<SearchObjectMock>(mocks, false);

            //SearchObjectMock foundMock = search.Search(mockObject);

            //// [Creating binary search with custom item]
        }

        void Method3()
        {
            // [Creating binary search with custom condition]

            //Collection<SearchObjectMock> mocks = new Collection<SearchObjectMock>();

            //string titleToFind = "";

            //Func<SearchObjectMock, SearchObjectMock, int> sortingComparsion = (mock1, mock2) =>
            //    {
            //        return mock1.Title.CompareTo(mock2.Title);
            //    };

            //BinarySearch<SearchObjectMock> search = new BinarySearch<SearchObjectMock>(mocks, false, sortingComparsion);

            //Func<SearchObjectMock, int> comparsion = (mock) =>
            //    {
            //        return titleToFind.CompareTo(mock.Title);
            //    };

            //SearchObjectMock foundMock = search.Search(comparsion);

            // [Creating binary search with custom condition]
        }
    }
}
