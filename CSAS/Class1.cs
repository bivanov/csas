﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csas
{
    /// \mainpage CSAS Documentation     
    /// Main aim is to create useful library for practical and educational purposes that contains some sorting, searching algorithms with corresponding data structures.
    ///
    /// Featured items:
    ///
    /// * linked list, believed to be lock-free
    /// * basic graph with some Dijkstra algorithms
    /// * binary search
    /// * heap
    /// * greatest common divider (yep, not so hard to implement :) )
    ///
    /// Contacts
    /// bogdanivanov at live.com
}
