[![alt text][2]][1] [![alt text][4]][3]


# CSAS - C Sharp Algorithms and Structures 

Main aim is to create useful library for practical and educational purposes that contains some sorting, searching algorithms with corresponding data structures.

### Featured items:

* linked list, believed to be lock-free
* basic graph with some Dijkstra algorithms
* binary search
* heap
* insertion, heap and sample sort

### Road map:

* skip list
* quick sort

### Installation

https://www.nuget.org/packages/CSAS/

### Documentation

http://bivanov.bitbucket.org/csas

### Contacts

[![alt text][6]][5]
bogdanivanov at live.com 

[1]: https://ci.appveyor.com/project/bivanov/csas
[2]: https://ci.appveyor.com/api/projects/status/peyqmrclffo1hr3j
[3]: https://www.nuget.org/packages/CSAS/
[4]: https://img.shields.io/nuget/v/CSAS.svg
[5]: https://ua.linkedin.com/in/bogdanivanov
[6]: https://static.licdn.com/scds/common/u/img/webpromo/btn_liprofile_blue_80x15.png